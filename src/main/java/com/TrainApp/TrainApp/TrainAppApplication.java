package com.TrainApp.TrainApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TrainAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(TrainAppApplication.class, args);
	}

	@GetMapping("/")
	public String hello() {
		return "Hello Spring Boot done via CI build tool!";
	}
	
	@GetMapping("/app")
	public String helloTwo() {
		return "Hey new version";
	}
}